jQuery(document).ready(function ($) {
    var width = $(window).width();
    if (width >= 991) {

        var wow = new WOW({
            boxClass: 'wow', // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 0, // distance to the element when triggering the animation (default is 0)
            mobile: true, // trigger animations on mobile devices (default is true)
            live: true, // act on asynchronously loaded content (default is true)
            callback: function (box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null // optional scroll container selector, otherwise use window
        });
        wow.init();
    }
    $('#header').toggleClass('nav-fixed', $(window).scrollTop() > 50);

    $(function () {
        $('a[title]').tooltip();
    });

    $('body').scrollspy({
        target: '#header',
        offset: 200
    });

    $(window).on('scroll', function () {
        $('#header').toggleClass('nav-fixed', $(window).scrollTop() > 50);
    });

    $('a.scrollto').on('click', function (e) {

        //store hash
        var target = this.hash;

        e.preventDefault();

        $('body').scrollTo(target, 800, {
            offset: -70,
            axis: 'y',
            easing: 'easeOutQuad'
        });

        //Collapse mobile menu after clicking
        if ($('.navbar-collapse').hasClass('in')) {
            $('.navbar-collapse').removeClass('in').addClass('collapse');
        }
    });

    readOptions();
    initRating();

    $('a[class="personal-data"]').on('click', function(e) {
        $.ajax({
            url: "/personaldata",
            type: "POST",
            cache: false,
            success: function (data) {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DEFAULT,
                    cssClass: 'personalDataModalLong',
                    title: 'Соглашение на обработку персональных данных',
                    message: data
                });
            },
            error: function (item) {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DEFAULT,
                    cssClass: 'personalDataModalLong',
                    title: 'Соглашение на обработку персональных данных',
                    message: 'Не удалось загрузить соглашение.'
                });
            }
        });
    });

    $('#formfeedback').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            // handle the invalid form...
        } else {
            e.preventDefault();
            $.ajax({
                url: "/sendapp",
                type: "POST",
                data: new FormData($('#formfeedback')[0]),
                cache: false,
                contentType: false,
                processData: false,
                xhr: function () {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        // For handling the progress of the upload
                        myXhr.upload.addEventListener('progress', function (e) {
                            if (e.lengthComputable) {
                                var percentComplete = e.loaded / e.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('.progress-bar').attr({
                                    'aria-valuenow': percentComplete,
                                    'style': 'width: ' + percentComplete + '%'
                                })

                                if (percentComplete === 100) {

                                }
                            }
                        }, false);
                    }
                    return myXhr;
                },
                success: function () {
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_SUCCESS,
                        title: 'Ваша заявка отправлена на рассмотрение',
                        message: 'При указании в анкете ссылки на социальные сети, убедитесь что у вас открыты личные сообщения. Ждите скорого ответа.'
                    });
                },
                error: function (item) {
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'Ой-ёй',
                        message: 'В данный момент мы не можем принять вашу заявку, попробуйте позже.'
                    });
                }
            });

            return false;
        }
    });

    $('#formreview').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            // handle the invalid form...
        } else {
            e.preventDefault();

            if ($('#stars').raty('score') == undefined) {
                $('.st').append('<ul class="list-unstyled"><li>Укажите оценку</li></ul>');
                return;
            }

            $.ajax({
                url: "/sendreview",
                type: "POST",
                data: $('#formreview').serialize(),
                success: function () {
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_SUCCESS,
                        title: 'Большое спасибо! :)',
                        message: 'В скором времени Ваш отзыв появится на главной странице сайта.'
                    });
                },
                error: function (item) {
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'Ой-ёй',
                        message: 'В данный момент мы не можем принять вашу заявку, попробуйте позже.'
                    });
                }
            });

            return false;
        }
    });


    $("#inputfile").fileinput({
        language: "ru",
        theme: "explorer",
        uploadUrl: "/sendapp",
        allowedFileExtensions: ["zip", "rar"],
        maxFileSize: 5120,
        uploadAsync: false,
        previewFileIcon: '<i class="fa fa-file"></i>',
        allowedPreviewTypes: null,
        maxFileCount: 1,
        initialPreviewAsData: false,
        showUpload: false,
        showClose: false,
        showPreview: false
    });

    $('#stars').on('click', function() {
        $('.st').children().remove();
    });

    $('#inputfile').on('fileuploaderror', function (event, data, msg) {
        $('ul[class="ul-error"]').children().remove();
        $('ul[class="ul-error"]').append('<li>' + msg + '</li>');
        $('#msg-error').removeClass('hidden');
    });

    $('.fileinput-remove-button').on('click', function() {
        $('#msg-error').addClass('hidden');
        $('ul[class="ul-error"]').children().remove();
    });
});

function initRating() {
    $('#stars').raty({
        starType: 'i',
        hints: ['Ужасно', 'Есть куда стремиться', 'Справились', 'Хорошо', 'Просто супер']
    });
}

function readOptions() {
    $.getJSON('/data/options.json', function (data) {
        $.each(data, function(key, value) {
            $('#options').append($("<option>", {
                value: key,
                text: value
            }));
        });
    });
}