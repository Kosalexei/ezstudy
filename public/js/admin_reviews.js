$(document).ready(function () {
  $.ajax({
    url: "/allreviews",
    type: "GET",
    dataType: "json",
    contentType: "application/json"
  }).done(function (data) {
    $.each(data, function (i, item) {
      var params = getParams(item);
      
      $(".row").prepend(getApp(item, params, false));
    });
  });
});

function getParams(item) {
  let params = {};
  let date = new Date(item.created);

  let day = date.getDate(),
   month = date.getMonth() + 1,
   year = date.getFullYear(),
   hours = date.getHours(),
   minutes = date.getMinutes();

  params.id = `<span class="label label-warning">${item._id}</span>`;
  params.reviewText = `<p><strong>Описание:</strong> ${item.reviewText}</p>`;
  params.score = `<p><strong>Оценка:</strong> ${item.score}</p>`
  params.status = item.isVerified ? 'panel-success' : 'panel-danger';
  params.solveStatus = item.isVerified ? '<span class="label label-success">Опубликован</span>' : '<span class="label label-danger">Ожидает проверки</span>';
  params.btn = item.isVerified ? `<button href="javascript:;" onClick="updateApp(this);" data-id=${item._id} type="button" class="btn btn-danger">Снять</button>` :
    `<button href="javascript:;" onClick="updateApp(this);" data-id=${item._id} type="button" class="btn btn-success">Опубликова</button>`;
  
  if(day < 10) day = '0' + day;
  if(month < 10) month = '0' + month;
  if(hours < 10) hours = '0' + hours;
  if(minutes < 10) minutes = '0' + minutes; 

  params.date = `<p><strong>Дата создания:</strong> ${day}.${month}.${year} ${hours}:${minutes}</p>`;

  return params;
}

function getApp(item, params, isUpdate) {
  var col = isUpdate ? '' : `<div class="col-sm-6 col-md-6" data-id=${item._id}>`;
  var endcol = isUpdate ? '' : '</div>';

  var app = `\
  ${col}
    <div class="panel ${params.status}">
      <div class="title panel-heading">
        <div class="leftstr">${item.firstName} ${item.secondName != null ? item.secondName : ''} ${params.id}</div>
        <div class="rightstr">${params.solveStatus}</div>
      </div>
      <div class="text panel-body">
        ${params.reviewText}${params.score}
        <hr>
        <div class="bottom_panel">
          <div class="master_info">${params.date}</div>
          <div class="toolbox">${params.btn}</div>
        </div>
      </div>
      
    </div>
  ${endcol}`;

  return app;
}

function updateApp(app) {
  $.ajax({
    url: `/review/${$(app).attr('data-id')}`,
    type: 'PUT',
    dataType: "json",
    contentType: "application/json",
    success: function (item) {
      var params = getParams(item);
      $(`div[data-id="${item._id}"]`).hide().html(getApp(item, params, true)).fadeIn('slow');
    }
  });
}