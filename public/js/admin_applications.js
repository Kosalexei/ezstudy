$(document).ready(function () {
  $.ajax({
    url: "/allapps",
    type: "GET",
    dataType: "json",
    contentType: "application/json"
  }).done(function (data) {
    $.each(data, function (i, item) {
      var params = getParams(item);
      
      $(".row").prepend(getApp(item, params, false));
    });
  });
});

function getParams(item) {
  let params = {};
  let date = new Date(item.created);

  let day = date.getDate(),
   month = date.getMonth() + 1,
   year = date.getFullYear(),
   hours = date.getHours(),
   minutes = date.getMinutes();

  params.id = `<span class="label label-warning">${item._id}</span>`;
  params.email = `<p><strong>Email:</strong> ${item.email}</p>`;
  params.social = item.social == null ? '' : `<p><strong>Социальная сеть:</strong> ${item.social}</p>`;
  params.description = `<p><strong>Описание:</strong> ${item.description}</p>`;
  params.subject = `<p><strong>Предмет:</strong> ${item.subject}</p>`;
  params.worktype = item.worktype == null ? '' : `<p><strong>Тип работы:</strong> ${getOptionName(item.worktype)}</p>`;
  params.status = item.isSolved ? 'panel-success' : 'panel-danger';
  params.solveStatus = item.isSolved ? '<span class="label label-success">Выполнено</span>' : '<span class="label label-danger">Не выполнено</span>';
  params.files = `<p><strong>Прикрепленные файлы:</strong></p>${getFiles(item.files)}`;
  params.btn = item.isSolved ? `<button href="javascript:;" onClick="updateApp(this);" data-id=${item._id} type="button" class="btn btn-danger">Не выполнено</button>` :
    `<button href="javascript:;" onClick="updateApp(this);" data-id=${item._id} type="button" class="btn btn-success">Выполнено</button>`;
  
  if(day < 10) day = '0' + day;
  if(month < 10) month = '0' + month;
  if(hours < 10) hours = '0' + hours;
  if(minutes < 10) minutes = '0' + minutes; 

  params.date = `<p><strong>Дата создания:</strong> ${day}.${month}.${year} ${hours}:${minutes}</p>`;

  return params;
}

function getApp(item, params, isUpdate) {
  var col = isUpdate ? '' : `<div class="col-sm-6 col-md-6" data-id=${item._id}>`;
  var endcol = isUpdate ? '' : '</div>';

  var app = `\
  ${col}
    <div class="panel ${params.status}">
      <div class="title panel-heading">
        <div class="leftstr">${item.firstName} ${item.secondName != null ? item.secondName : ''} ${params.id}</div>
        <div class="rightstr">${params.solveStatus}</div>
      </div>
      <div class="text panel-body">
        ${params.email}${params.social}${params.subject}${params.worktype}${params.description}${params.files}
        <hr>
        <div class="bottom_panel">
          <div class="master_info">${params.date}</div>
          <div class="toolbox">${params.btn}</div>
        </div>
      </div>
      
    </div>
  ${endcol}`;

  return app;
}

function getFiles(files) {
  let filestack = '';
  files.forEach(function(element) {
    filestack += `<a href="${element.destination}">${element.filename}</a> `;
  }, this);
  return filestack;
}

function updateApp(app) {
  $.ajax({
    url: `/app/${$(app).attr('data-id')}`,
    type: 'PUT',
    dataType: "json",
    contentType: "application/json",
    success: function (item) {
      var params = getParams(item);
      $(`div[data-id="${item._id}"]`).hide().html(getApp(item, params, true)).fadeIn('slow');
    }
  });
}

function getOptionName(key) {
  var options = {
    "lab": "Лабораторная",
    "course_work": "Курсовая",
    "graduate_work": "Диплом",
    "essay": "Реферат",
    "special": "Особый вид работы"
  }

  return options[key];
}