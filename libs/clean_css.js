const CleanCSS = require('clean-css');
const path = require('path');
const fs = require('fs');

module.exports = () => {
    let cssFiles = ['style.css', 'styleSlider.css', '_button.css', 'about.css', 'benefits.css'];
    let output = new CleanCSS().minify(pathCSS(cssFiles));

    // console.log(pathCSS(cssFiles));

    fs.writeFile(path.join(__dirname, '../public/css/style.min.css'), output.styles, function (err) {
        if (err) {
            return console.log(err);
        }

        console.log("The file was saved!");
    });

    // Функция генерации путей для CSS файлов
    function pathCSS(filenames) {
        filenames.forEach(function (element, index) {
            filenames[index] = path.join(__dirname, `../public/css/${element}`);
        }, this);

        return filenames;
    }
}