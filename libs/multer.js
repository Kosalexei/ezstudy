var path = require('path');
var fs = require('fs');
var multer = require('multer');

// var basefolder = 'C:/Users/kos_a/Desktop/Dev/ezstudy/uploads/';
var basefolder = path.join(__dirname, '../uploads/');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    let now = Date.now();
    let foldername = basefolder + now;
    if(!fs.existsSync(foldername)) {
      fs.mkdirSync(foldername);
    }
    cb(null, basefolder + now);
  },
  filename: function (req, file, cb) {
    // var name = file.originalname.replace(/\.[^/.]+$/, "");
    // var extension = path.extname(file.originalname);
    cb(null, file.originalname);
  }
});

var upload = multer({
  storage: storage
});

module.exports.upload = upload;