const mongoose = require('../libs/mongoose'),
  Schema = mongoose.Schema,
  autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

let reviewSchema = new Schema({
  firstName: {
    type: String,
    default: null
  },
  secondName: {
    type: String,
    default: null
  },
  reviewText: {
    type: String,
    default: null
  },
  isVerified: {
    type: Boolean,
    default: true
  },
  score: {
    type: Number,
    default: 0
  },
  personalData: {
    type: Boolean,
    default: false
  },
  created: {
    type: Date,
    default: Date.now
  }
});

// for autoincrement _id
reviewSchema.plugin(autoIncrement.plugin, {
  model: 'Review',
  startAt: 1
});

reviewSchema.statics.add = async function (firstName, secondName, reviewText, isVerified, score, personalData) {
  let Review = this;

  let review = new Review({
    firstName: firstName,
    secondName: secondName,
    reviewText: reviewText,
    isVerified: isVerified,
    score: score,
    personalData: personalData
  });

  try {
    await review.save();
  } catch (error) {
    throw error;
  }

  return review;
};

reviewSchema.statics.getAll = async function () {
  let Review = this;

  try {
    return await Review.find();
  } catch (error) {
    throw error;
  }
};

reviewSchema.statics.updateStatus = async function (id) {
  let Review = this;
  let review;

  try {
    review = await Review.findOne({ _id: id });
    review.isVerified = !review.isVerified;
    await review.save();
    return review;
  } catch (error) {
    throw error;
  }
};

exports.Review = mongoose.model('Review', reviewSchema);