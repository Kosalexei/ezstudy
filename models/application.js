const mongoose = require('../libs/mongoose'),
  Schema = mongoose.Schema,
  autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

let appSchema = new Schema({
  firstName: {
    type: String,
    default: null
  },
  secondName: {
    type: String,
    default: null
  },
  taskmaster: {
    type: String,
    default: null
  },
  email: {
    type: String,
    default: null
  },
  social: {
    type: String,
    default: null
  },
  worktype: {
    type: String,
    default: null
  },
  subject: {
    type: String,
    default: null
  },
  description: {
    type: String,
    default: null
  },
  isSolved: {
    type: Boolean,
    default: false
  },
  files: {
    type: Array,
    default: null
  },
  personalData: {
    type: Boolean,
    default: false
  },
  created: {
    type: Date,
    default: Date.now
  }
});

// for autoincrement _id
appSchema.plugin(autoIncrement.plugin, {
  model: 'Application',
  startAt: 1
});

appSchema.statics.add = async function (firstName, secondName, email, social, worktype, subject, description, files, personalData) {
  let Application = this;

  let application = new Application({
    firstName: firstName,
    secondName: secondName,
    email: email,
    social: social,
    worktype: worktype,
    subject: subject,
    description: description,
    files: files,
    personalData: personalData
  });

  try {
    await application.save();
  } catch (error) {
    throw error;
  }
  return application;
};

appSchema.statics.getAll = async function () {
  let Application = this;

  try {
    return await Application.find();
  } catch (error) {
    throw error;
  }
};

appSchema.statics.updateStatus = async function (id) {
  let Application = this;
  let app;

  try {
    app = await Application.findOne({_id: id});
    app.isSolved = !app.isSolved;
    await app.save();
    return app;
  } catch(error) {
    throw error;    
  }
};

exports.Application = mongoose.model('Application', appSchema);