var crypto = require('crypto');
var async = require('async');
var util = require('util');

var mongoose = require('../libs/mongoose'),
  Schema = mongoose.Schema,
  autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

var userSchema = new Schema({
  firstName: {
    type: String,
  },
  secondName: {
    type: String,
    default: null
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  hashedPassword: {
    type: String,
    required: true
  },
  salt: {
    type: String,
    required: true
  },
  personalData: {
    type: Boolean,
    default: false
  },
  created: {
    type: Date,
    default: Date.now
  }
});

// for autoincrement _id
userSchema.plugin(autoIncrement.plugin, {
  model: 'User',
  startAt: 1
});

userSchema.methods.encryptPassword = function (password) {
  return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

userSchema.virtual('password')
  .set(function (password) {
    this._plainPassword = password;
    this.salt = Math.random() + '';
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function () {
    return this._plainPassword;
  });

userSchema.methods.checkPassword = function (password) {
  return this.encryptPassword(password) === this.hashedPassword;
};

userSchema.statics.authorize = function (email, password, callback) {
  var User = this;

  async.waterfall([
    function (callback) {
      User.findOne({
        email: email
      }, callback);
    },
    function (user, callback) {
      if (user) {
        if (user.checkPassword(password)) {
          callback(null, user);
        } else {
          return callback(new AuthError("Неверный пароль."));
        }
      } else {
        return callback(new AuthError("Пользователь не найден."));
      }
    }
  ], callback);
};

userSchema.statics.register = function (email, firstName, secondName, password, callback) {
  var User = this;

  async.waterfall([
    function (callback) {
      User.findOne({
        email: email
      }, callback);
    },
    function (user, callback) {
      if (user) {
        return callback(new AuthError("E-mail уже используется."));
      } else {
        user = new User({
          email: email,
          firstName: firstName,
          secondName: secondName,
          password: password
        });

        user.save(function (err, user) {
          if (err) return callback(err);
          callback(null, user);
        });
      }
    }
  ], callback);
};

exports.User = mongoose.model('User', userSchema);

function AuthError(message) {
  Error.apply(this, arguments);
  Error.captureStackTrace(this, AuthError);

  this.message = message;
}

util.inherits(AuthError, Error);

AuthError.prototype.name = 'AuthError';

exports.AuthError = AuthError;