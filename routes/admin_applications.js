// Get Application page

exports.get = (req, res) => {
  res.render('sections/admin/applications', {
    page_name: 'applications'
  });
}