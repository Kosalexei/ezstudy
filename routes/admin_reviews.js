// Get Reviews page

exports.get = (req, res) => {
  res.render('sections/admin/reviews', {
    page_name: 'reviews'
  });
}