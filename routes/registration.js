var User = require('../models/user').User;
var HttpError = require('../error').HttpError;
var AuthError = require('../models/user').AuthError;

exports.get = function (req, res) {
	res.render('sections/admin/registration');
};

exports.post = function (req, res, next) {
	var email = req.body.email;
	var password = req.body.password;
	var firstName = req.body.firstName;
	var secondName = req.body.secondName;

	User.register(email, firstName, secondName, password, function (err, user) {
		if (err) {
			if (err instanceof AuthError) {
				return next(new HttpError(403, err.message));
			} else {
				return next(err);
			}
		}

		req.session.user = user._id;
		res.send({});
	});
};