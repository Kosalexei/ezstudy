/* GET home page. */
exports.get = function (req, res) {
  res.render('frontpage', {
    title: 'EzStudy'
  });
};