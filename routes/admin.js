/* GET admin page. */
exports.get = function (req, res) {
  if (!req.user) {
    res.redirect('/admin/login');
    return;
  }

  res.render('sections/admin/main', {
    page_name: 'main'
  });
}