const appRoutes = require('./app_routes');
const reviewRoutes = require('./review_routes');

module.exports = function(app) {
  app.get('/', require('./frontpage').get);
  app.get('/admin', require('./admin').get);  

  app.get('/admin/login', require('./login').get);
  app.post('/admin/login', require('./login').post);

  app.get('/admin/applications', require('./admin_applications').get);
  app.get('/admin/reviews', require('./admin_reviews').get);

  app.post('/admin/logout', require('./logout').post);

  app.post('/personaldata', require('./personal_data').post);

  app.get('/admin/registration', require('./registration').get);
  app.post('/admin/registration', require('./registration').post);
  
  appRoutes(app);
  reviewRoutes(app);
};