const Application = require('../models/application').Application;
const HttpError = require('../error').HttpError;
const mongoose = require('../libs/mongoose');
const upload = require('../libs/multer').upload;
const path = require('path');

module.exports = (app) => {
  // Add app
  app.post('/sendapp', upload.array('files', 5), async(req, res) => {
    let firstName = req.body.firstName;
    let secondName = req.body.secondName;
    let email = req.body.email;
    let social = req.body.social;
    let worktype = req.body.worktype;
    let subject = req.body.subject;
    let description = req.body.description;
    let files = req.files;
    let personalData = req.body.personalData == 'on' ? 'true' : 'false';

    // Modification file destination
    files.forEach(function (element) {
      element.destination = path.join('/', path.basename(path.dirname(element.destination)), '/', path.basename(path.dirname(element.path)), '/', element.originalname);
    }, this);

    if (mongoose.connection.readyState != 1) res.status(500).send('Something broke!');

    try {
      let application = await Application.add(firstName, secondName, email, social, worktype, subject, description, files, personalData);
      res.send(application);
    } catch (error) {
      res.send({
        'error': 'An error has occured'
      });
    }
  });

  //Read all apps
  app.get('/allapps', async(req, res) => {
    try {
      res.send(await Application.getAll());
    } catch (error) {
      res.send({
        'error': 'An error has occured'
      });
    }
  });

  // Update app status
  app.put('/app/:id', async(req, res) => {
    try {
      res.send(await Application.updateStatus(req.params.id));
    } catch (error) {
      res.send({
        'error': 'An error has occured'
      });
    }
  });

  // Downloads files
  app.get('/uploads/:path?/:filename?', (req, res) => {
    res.download(path.join(__dirname, '../uploads/', req.params.path, '/', req.params.filename));
  });
};