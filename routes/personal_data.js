var fs = require('fs');
var path = require('path');
var pathToFile = path.join(__dirname, '../public/data/personal_data.html');

exports.post = (req, res) => {
  fs.readFile(pathToFile, 'utf8', (err, data) => {
    if (err) {
      res.send({
        'error': 'An error has occured'
      });

      throw err;
    }

    data = data.replace(/(?:\r\n|\r|\n)/g, '');
    res.send(data);
  });
}