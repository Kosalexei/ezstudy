const Review = require('../models/review').Review;
const HttpError = require('../error').HttpError;
const mongoose = require('../libs/mongoose');

module.exports = (app) => {
  // Add app
  app.post('/sendreview', async(req, res) => {
    let firstName = req.body.firstName;
    let secondName = req.body.secondName;
    let reviewText = req.body.reviewText;
    let isVerified = req.body.isVerified;
    let score = req.body.score;
    let personalData = req.body.personalData == 'on' ? 'true' : 'false';

    if (mongoose.connection.readyState != 1) res.status(500).send('Something broke!');

    try {
      let review = await Review.add(firstName, secondName, reviewText, isVerified, score, personalData);
      res.send(review);
    } catch (error) {
      res.status(500).send('Something broke!');
    }
  });

  //Read all apps
  app.get('/allreviews', async(req, res) => {
    try {
      res.send(await Review.getAll());
    } catch (error) {
      res.send({
        'error': 'An error has occured'
      });
    }
  });

  // Update app status
  app.put('/review/:id', async(req, res) => {
    try {
      res.send(await Review.updateStatus(req.params.id));
    } catch (error) {
      res.send({
        'error': 'An error has occured'
      });
    }
  });
};