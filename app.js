const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const errorhandler = require('errorhandler');
const config = require('./config');
const compression = require('compression');

const HttpError = require('./error').HttpError;

const app = express();
require('./libs/clean_css')();

process.title = "ezstudy";
app.use(compression());

// view engine setup
app.engine('ejs', require('ejs-locals'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());

// var CleanCSS = require('clean-css');

// var output = new CleanCSS().minify([__dirname + '/public/css/style.css', __dirname + '/public/css/styleSlider.css', __dirname + '/public/css/_button.css', __dirname + '/public/css/about.css', __dirname + '/public/css/benefits.css']);

// var fs = require('fs');
// fs.writeFile(__dirname + '/public/css/style.min.css', output.styles, function(err) {
//     if(err) {
//         return console.log(err);
//     }

//     console.log("The file was saved!");
// }); 

var session = require('express-session');
var sessionStore = require('./libs/sessionStore');

app.use(session({
  secret: config.get('session:secret'),
  key: config.get('session:key'),
  cookie: config.get('session:cookie'),
  saveUninitialized: false,
  resave: false,
  store: sessionStore
}));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/js', express.static(__dirname + '/node_modules/jquery.scrollto'));
app.use('/js', express.static(__dirname + '/node_modules/jquery.easing'));
app.use('/js', express.static(__dirname + '/node_modules/raty-js/lib'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap-validator/dist'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap3-dialog/dist/js'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap-fileinput/js'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap-fileinput/themes'));
app.use('/js', express.static(__dirname + '/node_modules/wowjs/dist'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap-fileinput/css'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap3-dialog/dist/css'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/css', express.static(__dirname + '/node_modules/font-awesome/css'));
app.use('/css', express.static(__dirname + '/node_modules/wowjs/css/libs'));
app.use('/css', express.static(__dirname + '/node_modules/raty-js/lib'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap-fileinput/themes'));
app.use('/fonts', express.static(__dirname + '/node_modules/bootstrap/fonts'));
app.use('/img', express.static(__dirname + '/node_modules/bootstrap-fileinput/img'));


app.use(require('./middleware/sendHttpError'));
app.use(require('./middleware/loadUser'));

require('./routes')(app);

app.use(function(err, req, res, next) {
  if (typeof err == 'number') { // next(404);
      err = new HttpError(err);
  }

  if (err instanceof HttpError) {
      res.sendHttpError(err);
  } else {
      if (app.get('env') == 'development') {
          errorhandler()(err, req, res, next);
      } else {
          log.error(err);
          err = new HttpError(500);
          res.sendHttpError(err);
      }
  }
});

app.use(function (req, res) {
  res.status(404).send("Page not found.");
});

module.exports = app;