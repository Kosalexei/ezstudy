FROM node:8.9.4

ENV APP_HOME /ezstudy

RUN mkdir $APP_HOME
WORKDIR $APP_HOME
ADD . $APP_HOME

RUN npm install